<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190731093918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE language (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(2) NOT NULL, name VARCHAR(255) NOT NULL, is_active TINYINT(1) DEFAULT \'1\' NOT NULL, is_default TINYINT(0) DEFAULT \'1\' NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translation (id INT AUTO_INCREMENT NOT NULL, original VARCHAR(255) NOT NULL, hash VARCHAR(32) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE translation_mutation (id INT AUTO_INCREMENT NOT NULL, language_id INT DEFAULT NULL, translation_id INT DEFAULT NULL, singular VARCHAR(255) DEFAULT NULL, plural1 VARCHAR(255) DEFAULT NULL, plural2 VARCHAR(255) DEFAULT NULL, INDEX IDX_1C5BE73F82F1BAF4 (language_id), INDEX IDX_1C5BE73F9CAA2B25 (translation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE translation_mutation ADD CONSTRAINT FK_1C5BE73F82F1BAF4 FOREIGN KEY (language_id) REFERENCES language (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE translation_mutation ADD CONSTRAINT FK_1C5BE73F9CAA2B25 FOREIGN KEY (translation_id) REFERENCES translation (id) ON DELETE CASCADE');

        /* TRIGGERY */
        $this->addSql('
                        CREATE TRIGGER `language_auto_insert_mutation_by_language` AFTER INSERT ON `language` FOR EACH ROW
                        BEGIN
                            DECLARE done INT DEFAULT FALSE;
                            DECLARE in_translation_id INT;
                            DECLARE in_original VARCHAR(255);
                            DECLARE translations CURSOR FOR SELECT id, original FROM translation;
                            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

                            OPEN translations;
                                main_loop: LOOP
                                    FETCH translations INTO in_translation_id, in_original;

                                    IF done = 1 THEN
                                        LEAVE main_loop;
                                    END IF;

                                    INSERT INTO translation_mutation(language_id, translation_id, singular) VALUES (NEW.id, in_translation_id, in_original);
                                END LOOP;
                            CLOSE translations;
                        END;
                        ');
        $this->addSql('
                        CREATE TRIGGER `translation_auto_insert_mutation_by_language` AFTER INSERT ON `translation` FOR EACH ROW
                        BEGIN
                            DECLARE done INT DEFAULT FALSE;
                            DECLARE in_language_id INT;
                            DECLARE languages CURSOR FOR SELECT id FROM language;
                            DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;

                            OPEN languages;
                                main_loop: LOOP
                                    FETCH languages INTO in_language_id;

                                    IF done = 1 THEN
                                        LEAVE main_loop;
                                    END IF;

                                    INSERT INTO translation_mutation(language_id, translation_id, singular) VALUES (in_language_id, NEW.id, NEW.original);
                                END LOOP;
                            CLOSE languages;
                        END;
                        ');
        $this->addSql('
                        CREATE FUNCTION `create_translation`(`in_original` varchar(2048) CHARACTER SET \'utf8mb4\', `in_hash` varchar(32) CHARACTER SET \'utf8mb4\', `in_module` varchar(255) CHARACTER SET \'utf8mb4\', `in_language_id` int(11), `in_singular` varchar(2048) CHARACTER SET \'utf8mb4\', `in_plural1` varchar(2048) CHARACTER SET \'utf8mb4\', `in_plural2` varchar(2048) CHARACTER SET \'utf8mb4\') RETURNS tinyint(1)
                        BEGIN
                            INSERT INTO translation (original, hash, module, is_migration_insert) VALUES (in_original, in_hash, in_module, 1);
                            UPDATE translation_mutation SET language_id = in_language_id, singular = in_singular, plural1 = in_plural1, plural2 = in_plural2 WHERE translation_id = (SELECT id FROM translation  WHERE hash COLLATE utf8_unicode_ci = in_hash);
                            
                            RETURN 1;
                        END;
                        ');

        /* DATA */
        $this->addSql('INSERT INTO `language` (code, name, is_active, is_default) VALUES (?, ?, ?, ?)', ['cs', 'Čeština', 1, 1]);

        $resources = [
            [
                'name'        => 'translation',
                'title'       => 'role-resource.translation.title',
                'description' => 'role-resource.translation.description',
            ],
        ];

        foreach ($resources as $resource) {
            $this->addSql('INSERT INTO core_role_resource (name, title, description) VALUES (:name, :title, :description)', $resource);
        }
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation_mutation DROP FOREIGN KEY FK_1C5BE73F82F1BAF4');
        $this->addSql('ALTER TABLE translation_mutation DROP FOREIGN KEY FK_1C5BE73F9CAA2B25');
        $this->addSql('DROP TABLE language');
        $this->addSql('DROP TABLE translation');
        $this->addSql('DROP TABLE translation_mutation');
    }
}
