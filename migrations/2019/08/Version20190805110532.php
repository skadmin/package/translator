<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190805110532 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation CHANGE original original VARCHAR(2048) NOT NULL');
        $this->addSql('ALTER TABLE translation_mutation CHANGE singular singular VARCHAR(2048) DEFAULT NULL, CHANGE plural1 plural1 VARCHAR(2048) DEFAULT NULL, CHANGE plural2 plural2 VARCHAR(2048) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE translation CHANGE original original VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE translation_mutation CHANGE singular singular VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE plural1 plural1 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE plural2 plural2 VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
