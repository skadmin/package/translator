<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131093948 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $translations = [
            ['original' => 'translation.flash.delete-no-use.success %d', 'hash' => '2d3ee4b5ad9ed13d9d39061474ded87a', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nepoužité překlady byly smazány [%d].', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.translation.title', 'hash' => '26a2760971b94aa7a7735454a1edec11', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Překlady', 'plural1' => '', 'plural2' => ''],
            ['original' => 'role-resource.translation.description', 'hash' => 'c8c702ea5e76bf181a9e41379eae1b35', 'module' => 'admin', 'language_id' => 1, 'singular' => 'umožňuje překládat fráze napříč webem a jazyky', 'plural1' => '', 'plural2' => ''],
            ['original' => 'translation.without-translate.title', 'hash' => '3f95b0be13597963666f5f69d4b5f23b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nepřeložené fráze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.action.default', 'hash' => '7f85b6980e5b640b07d37204a59b644b', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Všechny překlady', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.flash.change-singular', 'hash' => 'fc1c9c3b4604242d525259058e97c21d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jednotné číslo bylo přeloženo.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'translation.default', 'hash' => 'f5458453c6e0c6ec84e65b39bae198c9', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Překlady', 'plural1' => '', 'plural2' => ''],
            ['original' => 'translation.default.title', 'hash' => 'e0d3748966ffa6536daf20d6c5ebc757', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Překlady', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.action.without-translate', 'hash' => '6814284c340b67e1e2a9042e050a9115', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Nepřeložené fráze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.action.delete-no-use', 'hash' => '9416279adb0be49f3393476d5164ed2e', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Smazat nepřeložené fráze', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.translation_original', 'hash' => '097770e95d2155f9bfff24863de62eee', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Originál', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.singular', 'hash' => '4be3a27250c19907d5617357caae9e2c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Jednotné číslo', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.plural1', 'hash' => '99fa19a283a9c59433ea8a5b7b00c288', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Množné číslo [2,3,4]', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.plural2', 'hash' => 'acb207c411e74512852ca4f727395df3', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Množné číslo [0,5-∞]', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.flash.change-plural1', 'hash' => 'af3c22c31f80eaa2460f21f045ebb1f8', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Množné číslo [2,3,4] bylo přeloženo.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.flash.change-plural2', 'hash' => '0e66c9ae0c82d50f968bf59d72077f1c', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Množné číslo [0,5-∞] bylo přeloženo.', 'plural1' => '', 'plural2' => ''],
            ['original' => 'grid.translation.module', 'hash' => '85d6471e0f979f6b4b074eac72a7f65d', 'module' => 'admin', 'language_id' => 1, 'singular' => 'Modul', 'plural1' => '', 'plural2' => ''],
        ];

        foreach ($translations as $translation) {
            $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
            $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
        }
    }

    public function down(Schema $schema): void
    {
    }
}
