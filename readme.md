### Usage

#### create new translation for migration

```sql
SELECT CONCAT("[",
              "'original' => '", original, "',",
              "'hash' => '", hash, "',",
              "'module' => '", module   , "',",
              "'language_id' => ", language_id, ",",
              "'singular' => '", singular, "',",
              "'plural1' => '", COALESCE(plural1, ""), "',",
              "'plural2' => '", COALESCE(plural2, ""), "'],") as result
FROM translation as t
         JOIN translation_mutation as tm ON t.id = tm.translation_id
WHERE original <> singular
  AND is_migration_insert = 0
  AND module = 'admin'
```

and after result use in foreach with sql function `create_translation(original, hash, module, languageId, singular, plural1, plural2);`

```php
$translation = [$resultSql];

foreach ($translations as $translation) {
    $this->addSql('DELETE FROM translation WHERE hash = :hash', $translation);
    $this->addSql('SELECT create_translation(:original, :hash, :module, :language_id, :singular, :plural1, :plural2)', $translation);
}
```