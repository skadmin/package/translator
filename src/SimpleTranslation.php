<?php

declare(strict_types=1);

namespace Skadmin\Translator;

use function array_merge;

final class SimpleTranslation
{
    private string $message = '';
    private int    $count   = 1;
    /** @var mixed[] */
    private array $parameters = [];

    /**
     * @param string|int|mixed[] $parameters
     */
    public function __construct(string $message, string|int|array $parameters, int $count = 1)
    {
        $this->message    = $message;
        $this->parameters = (array) $parameters;
        $this->count      = $count;
    }

    public static function create(string $message, string|int|array $parameters, int $count = 1): self
    {
        return new self($message, $parameters, $count);
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return mixed[]
     */
    public function getParameters(bool $withCount = false): array
    {
        if ($withCount) {
            return array_merge((array) $this->getCount(), $this->parameters);
        }

        return $this->parameters;
    }

    /**
     * @param mixed[] $parameters
     */
    public function setParameters(array $parameters): self
    {
        $this->parameters = $parameters;

        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }
}
