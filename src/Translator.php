<?php

declare(strict_types=1);

namespace Skadmin\Translator;

use Exception;
use Nette\Caching\Cache;
use Nette\Localization\ITranslator;
use Nette\Utils\Validators;
use Skadmin\Translator\Doctrine\Language\Language;
use Skadmin\Translator\Doctrine\Language\Translation;
use Skadmin\Translator\Doctrine\Language\TranslationMutation;
use Skadmin\Translator\Doctrine\User\LanguageFacade;

use function array_values;
use function assert;
use function in_array;
use function is_string;
use function md5;
use function preg_replace;
use function sprintf;
use function trim;
use function vsprintf;

final class Translator implements ITranslator
{
    public const CACHE_KEY_LANGUAGES = 'languages';
    public const CACHE_KEY_DEFAULT_LANGUAGE = 'default-language';

    /** @var TranslationMutation[][] */
    private array $translations = [];
    private LanguageFacade $facadeLanguage;
    private Cache $cache;
    private int $languageId;
    private string $module = 'core';

    public function __construct(LanguageFacade $facadeLanguage)
    {
        $this->facadeLanguage = $facadeLanguage;
        $this->cache = $this->facadeLanguage->getCache();
        $this->loadTranslations();

        $this->setLanguage();
    }

    private function loadTranslations(?int $languageId = null, bool $invalidate = false): void
    {
        if ($languageId !== null) {
            $this->loadTranslation($languageId, $invalidate);
        } else {
            foreach ($this->loadLanguages() as $language) {
                assert($language instanceof Language);
                if ($language->getId() === null) {
                    continue;
                }

                $this->loadTranslation($language->getId(), $invalidate);
            }
        }
    }

    private function loadTranslation(int $languageId, bool $invalidate = false): void
    {
        if ($invalidate) {
            $this->invalidate($languageId);
        }

        $this->translations[$languageId] = $this->cache->load($languageId, function (&$dependencies) use ($languageId) {
            $dependencies = [Cache::TAGS => ['translation']];
            $cacheStorage = [];
            foreach ($this->facadeLanguage->getMutationsForLanguage($languageId) as $mutation) {
                $this->addTranslationFromMutation($mutation, $cacheStorage);
            }

            return $cacheStorage;
        });
    }

    private function invalidate(?int $languageId = null): void
    {
        if ($languageId !== null) {
            $this->translations[$languageId] = [];
            $this->facadeLanguage->invalideCache($languageId);
        } else {
            $this->translations = [];

            foreach ($this->facadeLanguage->getLanguages() as $language) {
                $this->facadeLanguage->invalideCache($languageId);
            }
        }
    }

    /**
     * @param mixed[] $cacheStorage
     */
    private function addTranslationFromMutation(TranslationMutation $mutation, array &$cacheStorage): void
    {
        $cacheStorage[$mutation->getTranslation()->getHash()] = $mutation;
    }

    /**
     * @return Language[]
     */
    private function loadLanguages(): array
    {
        return $this->cache->load(self::CACHE_KEY_LANGUAGES, function (): array {
            //Debugger::barDump('load-L');
            return $this->facadeLanguage->getLanguages();
        });
    }

    public function setLanguage(?int $languageId = null): void
    {
        if ($languageId === null) {
            if ($this->loadDefaultLanguage()->getId() === null) {
                throw new Exception('Default language is not set!');
            }

            $this->languageId = $this->loadDefaultLanguage()->getId();
        } else {
            $this->languageId = $languageId;
        }
    }

    private function loadDefaultLanguage(): Language
    {
        return $this->cache->load(self::CACHE_KEY_DEFAULT_LANGUAGE, function (): ?Language {
            //Debugger::barDump('load-D-L');
            return $this->facadeLanguage->getLanguageDefault();
        });
    }

    public function setModule(string $module): void
    {
        $this->module = $module;
    }

    public function translate(mixed $message, mixed ...$parameters): string
    {
        if (!((is_string($message) && trim($message) !== '') || $message instanceof SimpleTranslation)) {
            return $message;
        }

        if ($message instanceof SimpleTranslation) {
            $parameters = $message->getParameters(true);
            $message = $message->getMessage();
        }

        $hashMessage = md5(sprintf('%s%s', $message, $this->module));
        if (!isset($this->translations[$this->languageId][$hashMessage])) {
            $translation = $this->facadeLanguage->createTranslation($message, $this->module, $hashMessage);
            $this->addTranslation($translation);
        }

        return $this->translateFormatted($this->translations[$this->languageId][$hashMessage], $parameters);
    }

    private function addTranslation(Translation $translation, ?int $languageId = null): void
    {
        foreach ($translation->getMutations() as $mutation) {
            if ($mutation->getLanguage()->getId() !== $languageId && $languageId !== null) {
                continue;
            }

            $this->translations[$mutation->getLanguage()->getId()][$translation->getHash()] = $mutation;

            // save new data to cache
            $this->cache->save(
                $mutation->getLanguage()->getId(),
                $this->translations[$mutation->getLanguage()->getId()],
                [Cache::TAGS => ['translation']]
            );
        }
    }

    /**
     * @param mixed[] $parameters
     */
    private function translateFormatted(TranslationMutation $mutation, array $parameters = []): string
    {
        if (isset($parameters[0]) && Validators::isNumericInt($parameters[0])) {
            $count = $parameters[0];
            unset($parameters[0]);
            $parameters = array_values($parameters);
        } else {
            $count = 1;
        }

        if (in_array($count, [null, 1], true)) {
            $phrase = $mutation->getSingular();
        } elseif (in_array($count, [2, 3, 4], true)) {
            $phrase = $mutation->getPlural1(true);
        } else {
            $phrase = $mutation->getPlural2(true);
        }

        $phrase = preg_replace('/([%][^sducoxXbeEfFgGhH])/', '%%', $phrase);

        try {
            return vsprintf($phrase, $parameters);
        } catch (\ValueError $e) {
            \Tracy\Debugger::barDump([$phrase, $parameters], 'translateFormatted');
            return $phrase;
        }
    }

    /**
     * @param mixed[] $parameters
     */
    public function getTranslation(string $message, array $parameters = [], int $count = 1): SimpleTranslation
    {
        return new SimpleTranslation($message, $parameters, $count);
    }
}
