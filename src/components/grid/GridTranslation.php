<?php

declare(strict_types=1);

namespace Skadmin\Translator\Components\Grid;

use App\CoreModule\Presenters\CorePresenter;
use App\Model\System\Constant;
use App\Model\System\Flash;
use Skadmin\Translator\Doctrine\User\LanguageFacade;
use SkadminUtils\GridControls\UI\GridDoctrine;

use function count;
use function dump;
use function intval;

class GridTranslation extends GridDoctrine
{
    private LanguageFacade $facade;

    public function __construct(CorePresenter $presenter, LanguageFacade $facade)
    {
        parent::__construct($presenter);
        $this->facade = $facade;
    }

    public function create(): GridDoctrine
    {
        // DEFAULT
        $this->setPrimaryKey('id');
        $this->setDataSource($this->facade->getModelMutation());

        // DATA
        $modulesArray      = $this->facade->getListOfModules();
        $languageCodeArray = [];
        foreach ($this->facade->getLanguages() as $language) {
            $languageCodeArray[$language->getId()] = $language->getCode();
        }

        // COLUMNS
        if (count($languageCodeArray) > 1) {
            $this->addColumnText('language_code', 'grid.translation.language_code', 'language.code')
                ->setAlign('center');
        }

        if (count($modulesArray) > 1) {
            $this->addColumnText('module', 'grid.translation.module', 'translation.module')
                ->setAlign('center');
        }

        $this->addColumnText('translation_original', 'grid.translation.translation_original', 'translation.original');
        $this->addColumnText('singular', 'grid.translation.singular')
            ->setTemplateEscaping(false);
        $this->addColumnText('plural1', 'grid.translation.plural1')
            ->setTemplateEscaping(false);
        $this->addColumnText('plural2', 'grid.translation.plural2')
            ->setTemplateEscaping(false);

        // EDIT IF TRANSLATION ALLOWED WRITE
        if ($this->presenter->getUser()->isAllowed('translation', 'write')) {
            $this->getColumn('singular')->setEditableCallback([$this, 'gridTranslationAdminChangeSingular']);
            $this->getColumn('plural1')->setEditableCallback([$this, 'gridTranslationAdminChangePlural1']);
            $this->getColumn('plural2')->setEditableCallback([$this, 'gridTranslationAdminChangePlural2']);
        }

        // FILTER
        if (count($languageCodeArray) > 1) {
            $this->addFilterSelect('language_code', 'grid.translation.language_code', Constant::PROMTP_ARR + $languageCodeArray, 'language.id');
        }

        if (count($modulesArray) > 1) {
            $this->addFilterSelect('module', 'grid.translation.module', Constant::PROMTP_ARR + $modulesArray, 'translation.module');
        }

        $this->addFilterText('translation_original', 'grid.translation.translation_original', 'translation.original')
            ->setSplitWordsSearch(false);
        $this->addFilterText('singular', 'grid.translation.singular')
            ->setSplitWordsSearch(false);
        $this->addFilterText('plural1', 'grid.translation.plural1')
            ->setSplitWordsSearch(false);
        $this->addFilterText('plural2', 'grid.translation.plural2')
            ->setSplitWordsSearch(false);

        // TOOLBAR

        // IF USER ALLOWED DELETE
        if ($this->presenter->getUser()->isAllowed('translation', 'write')) {
            $this->addToolbarButton('withoutTranslate', 'grid.translation.action.without-translate')
                ->setIcon('list-ul')
                ->setClass('btn btn-xs btn-primary');
        }

        if ($this->presenter->getUser()->isAllowed('translation', 'delete')) {
            $this->addToolbarButton('deleteNoUse!', 'grid.translation.action.delete-no-use')
                ->setIcon('eraser')
                ->setClass('btn btn-xs btn-outline-primary ajax');
        }

        return $this;
    }

    public function gridTranslationAdminChangeSingular(int|string $id, string $singular): void
    {
        $mutation = $this->facade->updateSingular(intval($id), $singular);
        $this->invalidateCache($mutation->getLanguage()->getId());
        $this->presenter->flashMessage('grid.translation.flash.change-singular', Flash::SUCCESS);
    }

    private function invalidateCache(?int $id): void
    {
        $this->facade->invalideCache($id);
    }

    public function gridTranslationAdminChangePlural1(int|string $id, string $plural1): void
    {
        $mutation = $this->facade->updatePlural1(intval($id), $plural1);
        $this->invalidateCache($mutation->getLanguage()->getId());
        $this->presenter->flashMessage('grid.translation.flash.change-plural1', Flash::SUCCESS);
    }

    public function gridTranslationAdminChangePlural2(int|string $id, string $plural2): void
    {
        $mutation = $this->facade->updatePlural2(intval($id), $plural2);
        $this->invalidateCache($mutation->getLanguage()->getId());
        $this->presenter->flashMessage('grid.translation.flash.change-plural2', Flash::SUCCESS);
    }

    public function handleDeleteNoUse(): void
    {
        dump('xx');
        exit;
    }
}
