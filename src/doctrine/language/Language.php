<?php

declare(strict_types=1);

namespace Skadmin\Translator\Doctrine\Language;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

use function boolval;

#[ORM\Entity]
class Language
{
    use Entity\Id;
    use Entity\Name;
    use Entity\IsActive;

    #[ORM\Column(length: 2)]
    private string $code;

    #[ORM\Column(options: ['default' => false])]
    private bool $isDefault = true;

    public function isDefault(): bool
    {
        return boolval($this->isDefault);
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setIsDefault(bool|int $isDefault): void
    {
        $this->isDefault = boolval($isDefault);
    }
}
