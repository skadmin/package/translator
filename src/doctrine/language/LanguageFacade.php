<?php

declare(strict_types=1);

namespace Skadmin\Translator\Doctrine\User;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Exception;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Utils\ArrayHash;
use Nette\Utils\Arrays;
use Nettrine\ORM\EntityManagerDecorator;
use Skadmin\Translator\Doctrine\Language\Language;
use Skadmin\Translator\Doctrine\Language\Translation;
use Skadmin\Translator\Doctrine\Language\TranslationMutation;
use SkadminUtils\DoctrineTraits\Facade;

use function assert;
use function intval;
use function md5;
use function sprintf;

final class LanguageFacade extends Facade
{
    private string $tableTranslation;
    private string $tableTranslationMutation;
    private Cache  $cache;

    public function __construct(EntityManagerDecorator $em, IStorage $cacheStorage)
    {
        parent::__construct($em);
        $this->table                    = Language::class;
        $this->tableTranslation         = Translation::class;
        $this->tableTranslationMutation = TranslationMutation::class;

        $this->cache = new Cache($cacheStorage, 'skadmin.translation');
    }

    public function getCache(): Cache
    {
        return $this->cache;
    }

    public function deleteNoUseTranslation(): int
    {
        if ($this->getLanguageDefault() === null) {
            throw new Exception('Default language is not set!');
        }

        $dql   = sprintf(
            'select t.id from %s t join %s m with t.id = m.translation where m.language = %d AND t.original = m.singular AND m.plural1 is null AND m.plural2 is null',
            $this->tableTranslation,
            $this->tableTranslationMutation,
            $this->getLanguageDefault()->getId()
        );
        $query = $this->em->createQuery($dql);

        $translationIds = Arrays::map($query->execute(), static function ($array) {
            return $array['id'];
        });

        $criteria = Criteria::create();
        $criteria->where(Criteria::expr()->in('t.id', $translationIds));

        $countDeleted = $this->em->createQueryBuilder()
            ->delete($this->tableTranslation, 't')
            ->addCriteria($criteria)
            ->getQuery()
            ->execute();

        $this->invalideCache();

        return $countDeleted;
    }

    public function invalideCache(mixed $key = null): void
    {
        if ($key === null) {
            $this->cache->clean([Cache::TAGS => ['translation']]);
        } else {
            $this->cache->remove($key);
        }
    }

    /**
     * @return Language[]
     */
    public function getLanguages(bool $onlyActive = false): array
    {
        $criteria = [];

        if ($onlyActive) {
            $criteria['isActive'] = intval($onlyActive);
        }

        return $this->em
            ->getRepository($this->table)
            ->findBy($criteria);
    }

    public function getLanguage(?int $id = null): Language
    {
        if ($id === null) {
            return new Language();
        }

        $language = $this->em
            ->getRepository($this->table)
            ->find($id);

        return $language ?? new Language();
    }

    public function getLanguageByCode(string $code): ?Language
    {
        return $this->em
            ->getRepository($this->table)
            ->findOneBy(['code' => $code]);
    }

    public function getLanguageDefault(): ?Language
    {
        $criteria = ['isDefault' => true];

        return $this->em
            ->getRepository($this->table)
            ->findOneBy($criteria);
    }

    public function createTranslation(string $original, string $module, ?string $_hash = null): Translation
    {
        $hash        = $_hash ?? md5(sprintf('%s%s', $original, $module));
        $translation = $this->getTranslationByHash($hash);
        assert($translation instanceof Translation);

        if ($translation->isLoaded()) {
            return $translation;
        }

        $translation->create($original, $module, $hash);

        $this->em->persist($translation);
        $this->em->flush();
        $this->em->detach($translation);

        return $this->getTranslation($translation->getId());
    }

    public function getTranslationByHash(string $hash): Translation
    {
        $criteria = ['hash' => $hash];

        $translation = $this->em
            ->getRepository($this->tableTranslation)
            ->findOneBy($criteria);

        return $translation ?? new Translation();
    }

    public function getTranslation(?int $id = null): Translation
    {
        if ($id === null) {
            return new Translation();
        }

        $translation = $this->em
            ->getRepository($this->tableTranslation)
            ->find($id);

        return $translation ?? new Translation();
    }

    /**
     * @return TranslationMutation[]
     */
    public function getMutationsForLanguage(int $languageId): array
    {
        $criteria = ['language' => $languageId];

        return $this->em
            ->getRepository($this->tableTranslationMutation)
            ->findBy($criteria);
    }

    public function updateMutation(int $id, string $singular, string $plural1, string $plural2): TranslationMutation
    {
        $mutation = $this->getMutation($id);
        $mutation->updateMutation($singular, $plural1, $plural2);

        $this->em->persist($mutation);
        $this->em->flush();

        return $mutation;
    }

    public function getMutation(int $id): TranslationMutation
    {
        $mutation = $this->em
            ->getRepository($this->tableTranslationMutation)
            ->find($id);

        return $mutation ?? new TranslationMutation();
    }

    public function updateSingular(int $id, string $singular): TranslationMutation
    {
        $mutation = $this->getMutation($id);
        $mutation->updateSingular($singular);

        $this->em->persist($mutation);
        $this->em->flush();

        return $mutation;
    }

    public function updatePlural1(int $id, string $plural1): TranslationMutation
    {
        $mutation = $this->getMutation($id);
        $mutation->updatePlural1($plural1);

        $this->em->persist($mutation);
        $this->em->flush();

        return $mutation;
    }

    public function updatePlural2(int $id, string $plural2): TranslationMutation
    {
        $mutation = $this->getMutation($id);
        $mutation->updatePlural2($plural2);

        $this->em->persist($mutation);
        $this->em->flush();

        return $mutation;
    }

    public function getModelMutation(): QueryBuilder
    {
        $joinTables = [
            'language'    => ArrayHash::from([
                'table'         => $this->table,
                'conditionType' => Join::WITH,
                'condition'     => sprintf('%s.language = %s.id', 'mutation', 'language'),
            ]),
            'translation' => ArrayHash::from([
                'table'         => $this->tableTranslation,
                'conditionType' => Join::WITH,
                'condition'     => sprintf('%s.translation = %s.id', 'mutation', 'translation'),
            ]),
        ];

        return $this->getModelWithJoinTable($this->tableTranslationMutation, 'mutation', $joinTables);
    }

    public function getModelMutationWithoutTranslate(): QueryBuilder
    {
        $joinTables = [
            'language'    => ArrayHash::from([
                'table'         => $this->table,
                'conditionType' => Join::WITH,
                'condition'     => sprintf('%s.language = %s.id', 'mutation', 'language'),
            ]),
            'translation' => ArrayHash::from([
                'table'         => $this->tableTranslation,
                'conditionType' => Join::WITH,
                'condition'     => sprintf('%s.translation = %s.id AND %s.singular = %s.original', 'mutation', 'translation', 'mutation', 'translation'),
            ]),
        ];

        return $this->getModelWithJoinTable($this->tableTranslationMutation, 'mutation', $joinTables);
    }

    /**
     * @return string[]
     */
    public function getListOfModules(): array
    {
        $_modules = $this->em
            ->createQueryBuilder()
            ->from($this->tableTranslation, 't')
            ->select('t.module')
            ->groupBy('t.module')
            ->getQuery()
            ->execute();

        $modules = [];
        foreach ($_modules as $module) {
            $modules[$module['module']] = $module['module'];
        }

        return $modules;
    }
}
