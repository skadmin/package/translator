<?php

declare(strict_types=1);

namespace Skadmin\Translator\Doctrine\Language;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
#[ORM\Cache(region: 'translation')]
class Translation
{
    use Entity\Id;

    #[ORM\Column(length: 2048)]
    private string $original;

    #[ORM\Column]
    private string $module;

    #[ORM\Column(length: 32)]
    private string $hash;

    #[ORM\Column(options: ['default' => false])]
    private bool $isMigrationInsert = false;

    /** @var ArrayCollection|Collection|TranslationMutation[] */
    #[ORM\OneToMany(targetEntity: TranslationMutation::class, mappedBy: 'translation')]
    private ArrayCollection|Collection|array $mutations;

    public function __construct()
    {
        $this->mutations = new ArrayCollection();
    }

    public function getOriginal(): string
    {
        return $this->original;
    }

    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @return TranslationMutation[]
     */
    public function getMutations(): array
    {
        return $this->mutations->toArray();
    }

    public function getHash(): string
    {
        return $this->hash;
    }

    public function isMigrationInsert(): bool
    {
        return $this->isMigrationInsert;
    }

    public function create(string $original, string $module, string $hash): void
    {
        $this->original = $original;
        $this->module   = $module;
        $this->hash     = $hash;
    }
}
