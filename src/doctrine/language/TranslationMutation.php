<?php

declare(strict_types=1);

namespace Skadmin\Translator\Doctrine\Language;

use Doctrine\ORM\Mapping as ORM;
use SkadminUtils\DoctrineTraits\Entity;

#[ORM\Entity]
class TranslationMutation
{
    use Entity\Id;

    #[ORM\Column(nullable: true, length: 2048)]
    private ?string $singular = null;

    #[ORM\Column(nullable: true, length: 2048)]
    private ?string $plural1 = null;

    #[ORM\Column(nullable: true, length: 2048)]
    private ?string $plural2 = null;

    #[ORM\ManyToOne(targetEntity: Language::class, cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Language $language;

    #[ORM\ManyToOne(targetEntity: Translation::class, inversedBy: 'mutations', cascade: ['persist'])]
    #[ORM\JoinColumn(onDelete: 'cascade')]
    private ?Translation $translation;

    public function getSingular(): string
    {
        return $this->singular ?? '';
    }

    public function getPlural1(bool $returnSingularWhenEmpty = false): string
    {
        if ($this->plural1 !== null && $this->plural1 !== '') {
            return $this->plural1;
        }

        if ($returnSingularWhenEmpty) {
            return $this->getSingular();
        }

        return '';
    }

    public function getPlural2(bool $returnSingularWhenEmpty = false): string
    {
        if ($this->plural2 !== null && $this->plural2 !== '') {
            return $this->plural2;
        }

        if ($returnSingularWhenEmpty) {
            return $this->getSingular();
        }

        return '';
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function getTranslation(): Translation
    {
        return $this->translation;
    }

    public function updateMutation(string $singular, string $plural1, string $plural2): void
    {
        $this->updateSingular($singular);
        $this->updatePlural1($plural1);
        $this->updatePlural2($plural2);
    }

    public function updateSingular(string $singular): void
    {
        $this->singular = $singular;
    }

    public function updatePlural1(string $plural1): void
    {
        $this->plural1 = $plural1;
    }

    public function updatePlural2(string $plural2): void
    {
        $this->plural2 = $plural2;
    }

    /**
     * @return array<mixed>
     */
    public function __serialize(): array
    {
        return [
            'id'          => $this->getId(),
            'singular'    => $this->getSingular(),
            'plural1'     => $this->getPlural1(),
            'plural2'     => $this->getPlural2(),
            'language'    => null,
            'translation' => null,
        ];
    }
}
